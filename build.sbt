

lazy val p1 = crossProject.in(file(".")).
  settings(
    name := "akka-cloud-common",
    organization := "com.solarsuit",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.12.0"
  ).
  jvmSettings(
    libraryDependencies++=Seq(
      "com.lihaoyi" %% "autowire" % "0.2.6",
      "io.suzaku" %% "boopickle" % "1.2.6"
    )
  ).
  jsSettings(
    libraryDependencies ++= Seq(
      "com.lihaoyi" %%% "autowire" % "0.2.6",
      "io.suzaku" %%% "boopickle" % "1.2.6"
    )
  )

// Needed, so sbt finds the projects
lazy val commonJVM = p1.jvm
lazy val commonJS = p1.js