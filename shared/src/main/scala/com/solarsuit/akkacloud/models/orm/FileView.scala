package com.solarsuit.akkacloud.models.orm

import java.sql.Timestamp

/**
  * Created by arsenii on 22.03.17.
  */
case class FileView(path:String, isDir:Boolean, changed:Long, link:Option[String], contentType: String){

  lazy val timestamp = new Timestamp(changed)

  lazy val name:String = {
    if(path==null || path.isEmpty)
      ""
    else {
      val index = path.lastIndexOf("/")
      if (index == -1) path else path.substring(index + 1)
    }
  }
}

