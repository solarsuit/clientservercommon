package com.solarsuit.akkacloud.controllers.helpers

import com.solarsuit.akkacloud.models.orm.FileView

/**
  * Created by arsenii on 15.04.17.
  */

trait Manage{

  def getFileList(path:String): Option[List[FileView]]
  def share(path:String): Option[String]
  def unShare(path:String): Option[Unit]
  def delete(path:String): Option[Unit]
  def mkdir(path:String): Option[Unit]
}
