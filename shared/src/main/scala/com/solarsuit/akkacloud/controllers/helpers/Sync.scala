package com.solarsuit.akkacloud.controllers.helpers

/**
  * Created by arsenii on 14.05.17.
  */
trait Sync {


  //get all files changed since last sync
  def getChanges(syncId:String):Iterable[String]

  //to track updates and deletitions, returns unique id
  def registerSyncedFolder(rootDirectory:String,computerName:Option[String]):String

}
