package com.solarsuit.akkacloud.controllers.helpers

import java.nio.ByteBuffer
import autowire._
import boopickle.Default._

/**
  * Created by arsenii on 13.05.17.
  */
abstract class WireAbsractClient extends Client[ByteBuffer,Pickler,Pickler] {

  override def read[Result](p: ByteBuffer)(implicit evidence$1: Pickler[Result]): Result = Unpickle[Result].fromBytes(p)

  override def write[Result](r: Result)(implicit evidence$2: Pickler[Result]): ByteBuffer = Pickle.intoBytes(r)

}
